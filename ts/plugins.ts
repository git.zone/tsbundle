// node native
import * as path from 'path';

export { path };

// pushrocks scope
import * as smartcli from '@pushrocks/smartcli';
import * as smartfile from '@pushrocks/smartfile';
import * as smartlog from '@pushrocks/smartlog';
import * as smartlogDestinationLocal from '@pushrocks/smartlog-destination-local';
import * as smartpath from '@pushrocks/smartpath';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartspawn from '@pushrocks/smartspawn';

export {
  smartcli,
  smartfile,
  smartlog,
  smartlogDestinationLocal,
  smartpath,
  smartpromise,
  smartspawn,
};
