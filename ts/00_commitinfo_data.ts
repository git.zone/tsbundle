/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@gitzone/tsbundle',
  version: '2.0.8',
  description: 'a bundler using rollup for painless bundling of web projects'
}
