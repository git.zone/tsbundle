export * from '../plugins.js';

import * as htmlMinifier from 'html-minifier';

export {
  htmlMinifier
}